// Import libraries
import { useEffect, useMemo } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import * as SplashScreen from "expo-splash-screen";
import { Provider } from "react-redux";

// Import types
import { RootStackParamList } from "./src/types/RootStackParamList";

// Import screen
import HomeScreen from "./src/screens/HomeScreen";
import CountDownScreen from "./src/screens/CountDownScreen";

// Import hooks
import useLoadFont from "./src/hooks/useLoadFont";

// Import store
import { store } from "./src/store";

const Stack = createNativeStackNavigator<RootStackParamList>();

// Keep the splash screen visible while we fetch resources
SplashScreen.preventAutoHideAsync();

export default function App() {
  const [fontsLoaded] = useLoadFont();

  // This is done because we might add more
  // flags in the future. So we
  // make it one place instead
  const appisReady = useMemo(() => {
    return fontsLoaded;
  }, [fontsLoaded]);

  useEffect(() => {
    const hideSplashScreen = async () => {
      if (appisReady) {
        await SplashScreen.hideAsync();
      }
    };

    hideSplashScreen();
  }, [appisReady]);

  return appisReady ? (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="CountDown" component={CountDownScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  ) : (
    <></>
  );
}
