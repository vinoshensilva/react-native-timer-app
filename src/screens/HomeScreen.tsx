// import libraries
import React from "react";
import {
  View,
  StyleSheet,
  StatusBar as ReactNativeStatusBar,
} from "react-native";
import type { NativeStackScreenProps } from "@react-navigation/native-stack";

// Import types
import { RootStackParamList } from "@Types/RootStackParamList";

// Import components
import TimeForm from "@Components/Time/TimeForm";

type Props = NativeStackScreenProps<RootStackParamList, "Home">;

const HomeScreen = ({ navigation }: Props) => {
  return (
    <View style={styles.container}>
      <View>
        <TimeForm
          onSubmit={({ hours, minutes, seconds }) => {
            navigation.navigate("CountDown", {
              time: {
                hours,
                minutes,
                seconds,
              },
            });
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000",
    alignItems: "center",
    justifyContent: "center",
    marginTop: ReactNativeStatusBar.currentHeight,
    flexDirection: "column",
    gap: 20,
  },
});

export default HomeScreen;
