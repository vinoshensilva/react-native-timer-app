// Import libraries
import React, { useEffect } from "react";
import { StatusBar } from "expo-status-bar";
import {
  View,
  StatusBar as ReactNativeStatusBar,
  StyleSheet,
  GestureResponderEvent,
} from "react-native";
import type { NativeStackScreenProps } from "@react-navigation/native-stack";

// Import components
import TimeDisplay from "@Components/CountDownScreen/TimeDisplay";

// Import types
import { RootStackParamList } from "@Types/RootStackParamList";

type Props = NativeStackScreenProps<RootStackParamList, "CountDown">;

const CountDownScreen = ({ navigation, route }: Props) => {
  useEffect(() => {
    if (!route.params.time) {
      navigation.navigate("Home");
    }
  }, [route.params.time]);

  if (!route.params.time) {
    return <></>;
  }

  const onHandlePress = (_: GestureResponderEvent) => {
    navigation.navigate("Home");
  };

  return (
    <View style={styles.container}>
      <TimeDisplay time={route.params.time} />
      <StatusBar hidden={false} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000",
    alignItems: "center",
    justifyContent: "center",
    marginTop: ReactNativeStatusBar.currentHeight,
  },
});

export default CountDownScreen;
