// Import libraries
import React from "react";
import { View, Text, TouchableOpacity, Alert } from "react-native";
import { useFormik } from "formik";
import { addDays, addHours, addMinutes, addSeconds } from "date-fns";

// Import components
import NumberInput from "@Components/Shared/NumberInput";

export interface ITimeFormValues {
  seconds: number;
  minutes: number;
  hours: number;
}

export interface ITimeFormProps {
  onSubmit: (val: ITimeFormValues) => void;
}

const initialValues: ITimeFormValues = {
  hours: 0,
  minutes: 0,
  seconds: 0,
};

const TimeForm = ({ onSubmit }: ITimeFormProps) => {
  const formik = useFormik<ITimeFormValues>({
    initialValues,
    onSubmit: ({ hours, minutes, seconds }: ITimeFormValues) => {
      if (!hours && !minutes && !seconds) {
        Alert.alert("Please input some values.");
        return;
      }

      let newDate = new Date();
      newDate = addSeconds(
        addMinutes(addHours(newDate, hours), minutes),
        seconds
      );

      onSubmit({
        hours,
        minutes,
        seconds,
      });
    },
  });

  return (
    <View style={{ flexDirection: "column", gap: 20, alignItems: "center" }}>
      <View style={{ flexDirection: "row", gap: 20 }}>
        <View>
          <Text
            style={{
              color: "white",
              fontSize: 20,
              fontFamily: "Poppins_700Bold",
            }}
          >
            HOURS
          </Text>
          <View
            style={{
              justifyContent: "center",
              display: "flex",
              alignItems: "center",
            }}
          >
            <NumberInput
              onChangeValue={(val) => {
                let hours = parseInt(val + "");

                if (hours > 24 || hours < 0) {
                  hours = 0;
                }

                formik.setFieldValue("hours", hours);
              }}
              value={(formik.values.hours + "").padStart(2, "0")}
            />
          </View>
        </View>
        <View>
          <Text
            style={{
              color: "white",
              fontSize: 20,
              fontFamily: "Poppins_700Bold",
            }}
          >
            MINUTES
          </Text>
          <View
            style={{
              justifyContent: "center",
              display: "flex",
              alignItems: "center",
            }}
          >
            <NumberInput
              onChangeValue={(val) => {
                let minutes = parseInt(val + "");

                if (minutes > 60 || minutes < 0) {
                  minutes = 0;
                }

                formik.setFieldValue("minutes", minutes);
              }}
              value={(formik.values.minutes + "").padStart(2, "0")}
            />
          </View>
        </View>
        <View>
          <Text
            style={{
              color: "white",
              fontSize: 20,
              fontFamily: "Poppins_700Bold",
            }}
          >
            SECONDS
          </Text>
          <View
            style={{
              justifyContent: "center",
              display: "flex",
              alignItems: "center",
            }}
          >
            <NumberInput
              onChangeValue={(val) => {
                let seconds = parseInt(val + "");

                if (seconds > 60 || seconds < 0) {
                  seconds = 0;
                }

                formik.setFieldValue("seconds", seconds);
              }}
              value={(formik.values.seconds + "").padStart(2, "0")}
            />
          </View>
        </View>
      </View>
      <TouchableOpacity
        onPress={() => {
          formik.submitForm();
        }}
        style={{
          backgroundColor: "white",
          borderRadius: 10,
          padding: 10,
          width: 200,
        }}
      >
        <Text
          style={{
            fontSize: 14,
            fontWeight: "600",
            fontFamily: "Poppins_500Medium",
            textAlign: "center",
          }}
        >
          Start CountDown
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default TimeForm;
