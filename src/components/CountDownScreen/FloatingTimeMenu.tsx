// Import libraries
import React from "react";
import { StyleSheet, View } from "react-native";

// Import components
import IconButton from "@Components/Shared/IconButton";

interface FloatingTimeMenuProps {
  isPaused?: boolean;
  showPause?: boolean;
  onClickPause?: () => void;
  showBackButton?: boolean;
  onClickBack?: () => void;
  showRestart?: boolean;
  onClickRestart?: () => void;
}

const FloatingTimeMenu = ({
  isPaused = false,
  onClickBack,
  showBackButton = true,
  onClickRestart,
  showRestart = true,
  onClickPause,
  showPause = true,
}: FloatingTimeMenuProps) => {
  return (
    <View
      style={{
        position: "absolute",
        top: "4%",
        right: "4%",
        display: "flex",
        flexDirection: "row",
        gap: 10,
      }}
    >
      {showPause ? (
        <IconButton
          onPress={onClickPause}
          name={isPaused ? "play-arrow" : "pause"}
        />
      ) : (
        <></>
      )}
      {showRestart ? (
        <IconButton name="sync" onPress={onClickRestart} />
      ) : (
        <></>
      )}
      <IconButton name="settings" onPress={onClickBack} />
      {showBackButton ? (
        <IconButton name="arrow-back" onPress={onClickBack} />
      ) : (
        <></>
      )}
    </View>
  );
};

export default FloatingTimeMenu;

const styles = StyleSheet.create({
  d: {},
});
