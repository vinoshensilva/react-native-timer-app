// Import libraries
import { View } from "react-native";
import React, { useEffect, useRef } from "react";
import { LinearGradient } from "expo-linear-gradient";
import { Audio } from "expo-av";

// Import components
import Container from "@Components/CountDownScreen/Container";
import Bar from "@Components/CountDownScreen/Bar";
import FloatingTimeMenu from "@Components/CountDownScreen/FloatingTimeMenu";

// Import hooks
import useTimer from "@Hooks/useTimer";
import { useTypedNavigation } from "@Hooks/useTypedNavigation";

// Import utils
import { clamp } from "@Utils/clamp";

// Import types
import { ITimeFormValues } from "@Components/Time/TimeForm";
import usePlaySound from "@Hooks/usePlaySound";
import { sounds } from "@Constants/sounds";

interface ITimeDisplayProps {
  time: ITimeFormValues;
}

const TimeDisplay = ({ time }: ITimeDisplayProps) => {
  const navigation = useTypedNavigation();

  const soundRef = useRef<Audio.Sound>();

  const [{ hours, minutes, seconds }, pause, isPaused, restart] = useTimer({
    time,
    delay: 2000,
  });

  const { sound } = usePlaySound({
    soundAsset: sounds["alarm-ring"],
  });

  useEffect(() => {
    sound?.setIsLoopingAsync(true);
    soundRef.current = sound;
  }, [sound]);

  useEffect(() => {
    const arr = [hours, minutes, seconds];

    if (arr.every((el) => !el && el === 0)) {
      sound?.playAsync();
    } else if (isPaused) {
      sound?.pauseAsync();
    }
  }, [hours, minutes, seconds, isPaused]);

  return (
    <>
      <View
        style={{
          flex: 1,
          alignItems: "flex-start",
          width: "100%",
          padding: 20,
        }}
      >
        {/* Hours Display */}
        <Container title="Hours" value={hours.toString().padStart(2, "0")}>
          <LinearGradient
            colors={["#FC990A", "#AD18F0"]}
            style={{
              width: `${clamp({ num: (hours / 24) * 100, max: 100, min: 0 })}%`,
              height: 50,
              borderRadius: 10,
              marginTop: 10,
            }}
            start={[0, 0]}
            end={[1, 0]}
          ></LinearGradient>
        </Container>

        {/* Minutes Display */}
        <Container title="Minutes" value={minutes.toString().padStart(2, "0")}>
          <Bar percentage={(minutes / 60) * 100} />
        </Container>

        {/* Seconds Display*/}
        <Container title="Seconds" value={seconds.toString().padStart(2, "0")}>
          <Bar percentage={(seconds / 60) * 100} />
        </Container>

        <FloatingTimeMenu
          isPaused={isPaused}
          onClickBack={() => {
            navigation.navigate("Home");
          }}
          onClickPause={() => {
            pause();
          }}
          onClickRestart={() => {
            restart();
          }}
        />
      </View>
    </>
  );
};

export default TimeDisplay;
