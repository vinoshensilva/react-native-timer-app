import React, { PropsWithChildren } from "react";
import { View, Text } from "react-native";

interface IContainerProps {
  title: string;
  value: string;
}

const Container = ({
  title,
  value,
  children,
}: PropsWithChildren<IContainerProps>) => {
  return (
    <View style={{ flex: 1, width: "100%" }}>
      <Text
        style={{
          fontSize: 40,
          color: "white",
          fontWeight: "800",
          fontFamily: "Poppins_500Medium",
        }}
      >
        {value}
      </Text>
      <Text
        style={{
          color: "white",
          fontSize: 28,
          fontWeight: "600",
          fontFamily: "Montserrat_400Regular_Italic",
        }}
      >
        {title}
      </Text>
      {children}
    </View>
  );
};

export default Container;
