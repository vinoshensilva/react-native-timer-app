import { View, ViewStyle } from "react-native";

const Bar = ({
  percentage,
  style = {},
}: {
  percentage: number;
  style?: ViewStyle;
}) => {
  return (
    <View
      style={{
        ...style,
        borderRadius: 10,
        height: 50,
        backgroundColor: "blue",
        marginTop: 10,
        width: `${percentage}%`,
      }}
    ></View>
  );
};

export default Bar;
