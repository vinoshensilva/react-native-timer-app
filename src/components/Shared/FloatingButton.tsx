// Import libraries
import { TouchableOpacity, TouchableOpacityProps } from "react-native";

const FloatingButton = ({
  style,
  children,
  ...rest
}: TouchableOpacityProps) => {
  return (
    <TouchableOpacity
      {...rest}
      style={{
        position: "absolute",
        top: "4%",
        right: "10%",
        borderRadius: 50,
        backgroundColor: "white",
        padding: 10,
        ...(typeof style === "object" ? style : {}),
      }}
    >
      {children}
    </TouchableOpacity>
  );
};

export default FloatingButton;
