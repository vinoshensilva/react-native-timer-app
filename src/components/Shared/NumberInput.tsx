// Import libraries
import { TextInput, TextInputProps } from "react-native";

const NumberInput = ({
  onChangeValue,
  ...rest
}: TextInputProps & {
  onChangeValue: (val: number) => void;
}) => {
  const onChangeText = (val: string) => {
    const numVal = parseFloat(val);

    if (!isNaN(numVal) && typeof numVal === "number") {
      onChangeValue(numVal);
    }
  };

  return (
    <TextInput
      {...rest}
      style={{
        color: "white",
        borderBottomWidth: 2,
        borderBottomColor: "white",
        marginBottom: 2,
        width: 50,
        fontSize: 40,
        textAlign: "center",
        fontFamily: "Poppins_500Medium",
      }}
      placeholderTextColor="white"
      onChangeText={onChangeText}
      keyboardType="numeric"
    />
  );
};

export default NumberInput;
