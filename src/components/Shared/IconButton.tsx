// Import libraries
import {
  TouchableOpacityProps,
  TouchableOpacity,
} from "react-native";
import React from "react";
import { MaterialIcons } from "@expo/vector-icons";

interface IconButtonProps {
  name: keyof typeof MaterialIcons.glyphMap;
}

const IconButton = ({
  name,
  style,
  ...rest
}: IconButtonProps & TouchableOpacityProps) => {
  return (
    <TouchableOpacity
      style={{
        borderRadius: 50,
        backgroundColor: "white",
        padding: 10,
        ...(typeof style === "object" ? style : {}),
      }}
      {...rest}
    >
      <MaterialIcons name={name} size={20} color="black" />
    </TouchableOpacity>
  );
};

export default IconButton;
