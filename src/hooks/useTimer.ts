// Import libraries
import { ITimeFormValues } from "@Components/Time/TimeForm";
import { differenceInSeconds, getTime } from "date-fns";
import { useState, useEffect, useRef } from "react";
import { convertTimeToMilliseconds } from "../utils/time";

const defaultData: ITimeFormValues = {
  hours: 0,
  minutes: 0,
  seconds: 0,
};

const DAY_AS_SECONDS = 86400;
const HOURS_AS_SECONDS = 3600;
const MINUTES_AS_SECONDS = 60;

const formatSecondsDiff = (diff: number) => {
  const days = Math.floor(diff / DAY_AS_SECONDS);
  const hours = Math.floor((diff - days * DAY_AS_SECONDS) / HOURS_AS_SECONDS);
  const minutes = Math.floor(
    (diff - days * DAY_AS_SECONDS - hours * HOURS_AS_SECONDS) /
      MINUTES_AS_SECONDS
  );
  const seconds =
    diff -
    days * DAY_AS_SECONDS -
    hours * HOURS_AS_SECONDS -
    minutes * MINUTES_AS_SECONDS;

  return {
    days,
    hours,
    minutes,
    seconds,
  };
};

// Returns back seconds
const useTimer = ({
  time,
  //   Default is every 1 second
  interval = 1000,
  delay = 0,
}: {
  time: ITimeFormValues;
  interval?: number;
  delay?: number;
}): [
  value: ITimeFormValues,
  pause: () => void,
  isPaused: boolean,
  restart: () => void
] => {
  const timerRef = useRef<number>(0);
  const timerId = useRef<NodeJS.Timer | null>(null);
  const [timeLeft, setTimeLeft] = useState<ITimeFormValues>(defaultData);
  const [passedTime, setPassedTime] = useState<ITimeFormValues>(time);
  const [isPaused, setIsPaused] = useState<boolean>(false);

  const start = (time: ITimeFormValues) => {
    const targetDate = convertTimeToMilliseconds(time);

    if (targetDate.getTime() > 0) {
      endInterval();

      const diff = differenceInSeconds(targetDate, new Date());
      setTimeLeft(time);
      timerRef.current = diff;

      setIsPaused(false);
      setTimeout(() => {
        const tempTimerId = setInterval(() => {
          if (timerRef.current - 1 === 0) {
            endInterval();
            setTimeLeft(defaultData);
          } else {
            timerRef.current -= 1;
            setTimeLeft(formatSecondsDiff(timerRef.current));
          }
        }, interval);

        timerId.current = tempTimerId;
      }, delay);
    }
  };

  const restart = () => {
    start(time);
  };

  const endInterval = () => {
    if (timerId.current) {
      clearInterval(timerId.current);
    }
  };

  const pause = () => {
    if (timerId.current && timeLeft) {
      setIsPaused(true);
      clearInterval(timerId.current);
      timerId.current = null;
    } else if (timerRef.current) {
      start(timeLeft);
    }
  };

  useEffect(() => {
    start(passedTime);

    return () => {
      if (timerId.current) {
        clearInterval(timerId.current);
      }
    };
  }, [passedTime, interval]);

  return [timeLeft, pause, isPaused, restart];
};

export default useTimer;
