// Import libraries
import { useEffect, useState } from "react";
import { Audio } from "expo-av";

const usePlaySound = ({ soundAsset }: { soundAsset: any }) => {
  const [sound, setSound] = useState<Audio.Sound>();

  useEffect(() => {
    const loadSound = async () => {
      if (soundAsset) {
        const { sound } = await Audio.Sound.createAsync(soundAsset);
        setSound(sound);
      }
    };

    loadSound();
  }, []);

  useEffect(() => {
    return sound
      ? () => {
          sound.unloadAsync();
        }
      : undefined;
  }, [sound]);

  return {
    sound,
  };
};

export default usePlaySound;
