// Import libraries
import {
  NavigationProp,
  useNavigation as useReactNavigation,
} from "@react-navigation/native";

// Import types
import { RootStackParamList } from "@Types/RootStackParamList";

export const useTypedNavigation = () => {
  return useReactNavigation<NavigationProp<RootStackParamList>>();
};
