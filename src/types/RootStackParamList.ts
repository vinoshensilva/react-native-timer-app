// Import types or interface
import { ITimeFormValues } from "@Components/Time/TimeForm";

export type RootStackParamList = {
  Home: undefined;
  CountDown: {
    time: ITimeFormValues
  };
};
