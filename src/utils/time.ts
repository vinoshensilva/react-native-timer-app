// Import libraries
import { addHours, addMinutes, addSeconds } from "date-fns";

// Import types
import { ITimeFormValues } from "@Components/Time/TimeForm";

export const convertTimeToMilliseconds = ({
  seconds,
  hours,
  minutes,
}: ITimeFormValues) => {
  const newDate = new Date();

  return addSeconds(
    addMinutes(addHours(newDate, hours), minutes),
    seconds
  );
};
