export const clamp = ({
  min,
  max,
  num,
}: {
  min: number;
  max: number;
  num: number;
}) => Math.min(Math.max(num, min), max);
